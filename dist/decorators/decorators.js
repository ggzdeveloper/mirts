"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMethodMetadata = exports.getFieldsMetadata = exports.Controller = exports.Route = exports.entity = exports.field = void 0;
require("reflect-metadata");
function field(options) {
    return (target, propertyName) => {
        let _val = target[propertyName];
        var t = Reflect.getMetadata("design:type", target, propertyName);
        let type = t.name;
        const getter = () => {
            return _val;
        };
        const setter = newVal => {
            _val = newVal;
        };
        Object.defineProperty(target, propertyName, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
        if (!target.__fields__) {
            target.__fields__ = {};
        }
        target.__fields__[propertyName] = {
            type,
            ...options
        };
        appendFieldMetadata({ [propertyName]: { type, ...options } }, target);
    };
}
exports.field = field;
function entity() {
    return function (constructor) {
        //constructor.__proto__.__entity=true;
        let fields = getFieldsMetadata(new constructor({}, { isNewRecord: false }));
        //console.log(constructor.name);
        //constructor.__proto__[constructor.name] = fields;
        //constructor.__proto__.fields = fields;
        //console.log(constructor.name,fields);
        //return (new constructor()).constructor
        return class extends constructor {
            constructor() {
                super(...arguments);
                this.__entity = true;
                this.__fields = fields;
            }
        };
    };
}
exports.entity = entity;
function Route(options) {
    return function (target, propertyKey, descriptor) {
        appendMethodMetadata({ [propertyKey]: { ...options } }, target);
        /*
        let _val = target[propertyName];

        var t = Reflect.getMetadata("design:type", target, propertyName);
        
        //console.log(`${propertyName} type: ${t.name}`);
        let type = t.name;
        
        if (!target.__fields__) {
            target.__fields__ = {};
        }
        
        target.__fields__[propertyName] = {
            type,
            ...options
        };
        /*
        Reflect.defineMetadata("model:fields",target.__fields__,target);*/
        //appendFieldMetadata({[propertyName]:{type,...options}},target);
    };
}
exports.Route = Route;
function Controller(opt) {
    return function (constructor) {
        //constructor.__proto__.__controller=true;
        let fields = getMethodMetadata(new constructor());
        return class extends constructor {
            constructor() {
                super(...arguments);
                this.__controller = true;
                this.__path = opt.path;
                this.__MethodPath = fields;
                this.__permissions = opt.permissions || ['NONE'];
                this.__guards = opt.guards || [];
            }
            __render(req, res) {
                res.send(`<h1>${constructor.name}</h1>`);
            }
        };
    };
}
exports.Controller = Controller;
function getFieldsMetadata(target) {
    return Reflect.getMetadata("model:fields", target);
}
exports.getFieldsMetadata = getFieldsMetadata;
function getMethodMetadata(target) {
    return Reflect.getMetadata("model:Method", target);
}
exports.getMethodMetadata = getMethodMetadata;
function appendMethodMetadata(value, target) {
    let name = "model:Method";
    let data = Reflect.getMetadata(name, target);
    if (!data) {
        data = {};
    }
    //target.constructor.prototype.Method = {...data,...value};
    Reflect.defineMetadata(name, { ...data, ...value }, target);
}
function appendFieldMetadata(value, target) {
    let name = "model:fields";
    let data = Reflect.getMetadata(name, target);
    if (!data) {
        data = {};
    }
    target.constructor.prototype.fields = { ...data, ...value };
    Reflect.defineMetadata(name, { ...data, ...value }, target);
}
