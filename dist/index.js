"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Route = exports.Controller = exports.entity = exports.MJSON = exports.MText = exports.Model = exports.field = void 0;
const decorators_1 = require("./decorators/decorators");
Object.defineProperty(exports, "field", { enumerable: true, get: function () { return decorators_1.field; } });
Object.defineProperty(exports, "entity", { enumerable: true, get: function () { return decorators_1.entity; } });
Object.defineProperty(exports, "Controller", { enumerable: true, get: function () { return decorators_1.Controller; } });
Object.defineProperty(exports, "Route", { enumerable: true, get: function () { return decorators_1.Route; } });
const Model_1 = require("./models/Model");
Object.defineProperty(exports, "Model", { enumerable: true, get: function () { return Model_1.Model; } });
const Types_1 = require("./models/Types");
Object.defineProperty(exports, "MText", { enumerable: true, get: function () { return Types_1.MText; } });
Object.defineProperty(exports, "MJSON", { enumerable: true, get: function () { return Types_1.MJSON; } });
const MysqlStorage_1 = require("./datastorage/MysqlStorage");
const fs = require("fs");
const path = require("path");
const ValidateModels_1 = require("./functions/ValidateModels");
const exportModels_1 = require("./functions/exportModels");
const httpServer_1 = require("./plugins/httpServer");
const getController_1 = require("./functions/getController");
const sequelize_1 = require("sequelize");
const getMiddlewares_1 = require("./functions/getMiddlewares");
class tsnodejs {
    constructor() {
        this.dirWorking = "";
    }
    async init() {
        tsnodejs.addConection('mysql', MysqlStorage_1.MysqlStorage);
        this.dirWorking = process.cwd();
        await this.processDirWorking();
        await this.getConfigurations();
        await this.initDataStorage();
        await this.initHttpServer();
    }
    async processDirWorking() {
        let dirConfig = path.resolve(this.dirWorking, "tsnodejs.json");
        if (fs.existsSync(dirConfig)) {
            let fconfig = fs.readFileSync(dirConfig, { encoding: 'utf8' });
            fconfig = JSON.parse(fconfig);
            if (fconfig.dirWorking) {
                this.dirWorking = path.resolve(this.dirWorking, fconfig.dirWorking);
            }
        }
    }
    async getConfigurations() {
        let dir = this.dirWorking + "/configs/";
        let files = fs.readdirSync(path.join(dir));
        let configs = {};
        files.forEach(file => {
            if (fs.lstatSync(path.join(dir, file)).isFile() && (path.join(dir, file).endsWith(".js") || path.join(dir, file).endsWith(".ts"))) {
                if (!path.parse(file).name.endsWith(".d")) {
                    let configFiles = require(path.join(dir, file));
                    let conf = configFiles.default;
                    let name = file.substring(0, file.length - 3);
                    configs[name] = conf;
                }
            }
        });
        Object.assign(tsnodejs.configs, configs);
    }
    async initHttpServer() {
        let http = new httpServer_1.default(tsnodejs.configs.http);
        await http.init();
        let middlewares = getMiddlewares_1.getAllMiddlewares(this.dirWorking + "/middlewares/");
        middlewares.forEach(e => {
            http.addMiddeleware(e);
        });
        let controllers = getController_1.getAllController(this.dirWorking + "/controllers/");
        controllers.forEach(e => {
            /* http.addRoute('get',`/${e.instance.__path}`,e.instance.__render); */
            for (let k in e.instance.__MethodPath) {
                let __permissions = JSON.parse(JSON.stringify(e.instance.__permissions));
                let conf = e.instance.__MethodPath[k];
                if (conf.permissions) {
                    conf.permissions.forEach(e => {
                        __permissions.push(e);
                    });
                }
                http.addRoute(conf.method, `/${e.instance.__path}/${conf.path}`, e.instance[k], __permissions);
            }
        });
    }
    async initDataStorage() {
        let dir = this.dirWorking + "/models/";
        let datastorageConfig = tsnodejs.configs.datastorage;
        if (datastorageConfig && datastorageConfig.driver && datastorageConfig.driver != "none") {
            let models = ValidateModels_1.getAllModels(dir);
            let driverName = datastorageConfig.driver;
            if (!tsnodejs.Connections[driverName]) {
                console.error("Driver not found, datastorage disabled");
            }
            else {
                let con = tsnodejs.Connections[driverName];
                let driver = new sequelize_1.Sequelize({
                    dialect: 'mysql',
                    host: datastorageConfig.host,
                    username: datastorageConfig.user,
                    password: datastorageConfig.password,
                    database: datastorageConfig.database,
                    port: datastorageConfig.port,
                    logging: datastorageConfig.logging || false,
                    pool: datastorageConfig.pool || {
                        max: 10,
                        min: 0,
                        acquire: 30000,
                        idle: 10000
                    }
                });
                Model_1.Model.datastorage = driver;
                try {
                    await exportModels_1.Migrate(models, driver, datastorageConfig.migrate);
                }
                catch (ex) {
                    console.error(ex);
                    throw ex;
                }
            }
        }
    }
    static async addConection(driver, datastorage) {
        this.Connections[driver] = datastorage;
    }
}
exports.default = tsnodejs;
tsnodejs.Connections = {};
tsnodejs.configs = {};
