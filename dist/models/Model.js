"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Model = void 0;
const sequelize_1 = require("sequelize");
class Model extends sequelize_1.Model {
    static _getFieldCreate() {
        let instance = new this({}, { isNewRecord: false });
        let fields = instance.__fields;
        let flds = {};
        for (let name in fields) {
            const options = fields[name];
            flds[name] = {};
            if (options.customType) {
                flds[name].type = options.customType;
                continue;
            }
            switch (options.type) {
                case 'Number':
                    flds[name].type = sequelize_1.DataTypes.INTEGER;
                    break;
                case 'MText':
                    flds[name].type = sequelize_1.DataTypes.TEXT({ length: 'long' });
                    break;
                case 'Date':
                    flds[name].type = sequelize_1.DataTypes.DATE;
                    break;
                case 'String':
                    flds[name].type = sequelize_1.DataTypes.STRING;
                    break;
                case 'Boolean':
                    flds[name].type = sequelize_1.DataTypes.BOOLEAN;
                    break;
                case 'MJSON':
                    flds[name].type = sequelize_1.DataTypes.TEXT({ length: 'long' });
                    break;
                default:
                    console.warn(`type [${options.type}] not reconize`);
                    continue;
                    break;
            }
            if (options.primary) {
                flds[name].primaryKey = true;
                //f += " PRIMARY KEY ";
            }
            if (options.autoIncrement) {
                flds[name].autoIncrement = true;
            }
            if (options.unique) {
                flds[name].unique = true;
            }
            if (options.uniqueGroup) {
                flds[name].unique = options.uniqueGroup;
            }
        }
        ;
        return flds;
    }
}
exports.Model = Model;
