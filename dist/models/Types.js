"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MJSON = exports.MText = void 0;
/**
 * Column definition Text
 */
class MText {
    static set(value) {
        return value;
    }
    static get(value) {
        return value;
    }
}
exports.MText = MText;
/**
 * Column definition JSON
 */
class MJSON {
    static set(value) {
        return JSON.stringify(value || "null");
    }
    static get(value) {
        return JSON.parse(value);
    }
}
exports.MJSON = MJSON;
