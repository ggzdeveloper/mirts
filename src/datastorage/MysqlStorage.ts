import * as mysql from 'mysql';
import { datastorage } from "../interface/datastorage";
import { DatabaseConfig } from '../interface/databaseConfig';

export class MysqlStorage implements datastorage {
    
    private conn: any;
	
    connect(options: DatabaseConfig): Promise<void> {
        return new Promise((d:Function, r) => {
            this.conn = mysql.createPool(options);
            d();
        })
    }

    find(tablename: string, where: any, options: any): Promise<Array<any>> {
        let sql = "";
        sql = `SELECT * FROM \`${tablename.toLowerCase()}\``;
        let whereParam = [];
        let params = {}
        if(where){
            for(let k in where){
                
                let kname =  k.replace(/\%/g,'');
                if(where[k] === undefined){
                    continue
                }
                if(k.startsWith("%")){
                    whereParam.push(`\`${kname}\` LIKE :${kname}`);
                }else{
                    whereParam.push(`\`${kname}\`=:${kname}`);
                }
                params[kname] = where[k];
            }
            if(whereParam.length){
                sql += ` WHERE ${whereParam.join(' AND ')}`;
            }
        }
        return this.nativeQuery(sql,params);
    }
    async findOne(tablename: string, where: any, options: any): Promise<any> {
        let sql = "";
        sql = `SELECT * FROM \`${tablename.toLowerCase()}\``;
        let whereParam = [];
        let params = {}
        if(where){
            for(let k in where){
                let kname =  k.replace(/\%/g,'');
                if(where[k] === undefined){
                    continue
                }
                if(k.startsWith("%")){
                    whereParam.push(`\`${kname}\` LIKE :${kname}`);
                }else{
                    whereParam.push(`\`${kname}\`=:${kname}`);
                }
                params[kname] = where[k];
            }
            if(whereParam.length){
                sql += ` WHERE ${whereParam.join(' AND ')}`;
            }
        }
        let r = await this.nativeQuery(sql,params)
        return r[0];
    }
    async count(tablename: string): Promise<number> {
        let r = await this.nativeQuery(`SELECT COUNT(*) as c FROM \`${tablename.toLowerCase()}\``,{});
        return Number(r[0].c);
    }
    async create(tablename: string, options: any): Promise<any> {
        let sql = `INSERT INTO \`${tablename.toLowerCase()}\` SET`;
        let flds = [];
        let row = {}
        for (let k in options) {
            if(options[k] === undefined){
                continue
            }
            flds.push(`\`${k}\`=:${k}`);
            row[k] = options[k];
        }
        return await this.nativeQuery(`${sql} ${flds.join(',')}`, row);
    }
    async update(tablename: string, where: any, options: any): Promise<any> {
        let sql = `UPDATE \`${tablename.toLowerCase()}\` SET`;
        let flds = [];
        let wflds = [];
        let row = {}
        for (let k in options) {
            if(options[k] === undefined){
                continue
            }
            flds.push(`\`${k}\`=:${k}`);
            row[k] = options[k];
        }
        for (let k in where) {
            if(where[k] === undefined){
                continue
            }
            wflds.push(`\`${k}\`=:w_${k}`);
            row[`w_${k}`] = where[k];
        }
        return await this.nativeQuery(`${sql} ${flds.join(',')} WHERE ${wflds.join(' AND ')}`, row);
    }
    async destroy(tablename: string, where: any, options: any): Promise<any> {
        let sql = `DELETE FROM \`${tablename.toLowerCase()}\``;
        let whereParam = [];
        let params = {}
        if(where){
            for(let k in where){
                let kname =  k.replace(/\%/g,'');
                if(where[k] === undefined){
                    continue
                }
                if(k.startsWith("%")){
                    whereParam.push(`\`${kname}\` LIKE :${kname}`);
                }else{
                    whereParam.push(`\`${kname}\`=:${kname}`);
                }
                params[kname] = where[k];
            }
            if(whereParam.length){
                sql += ` WHERE ${whereParam.join(' AND ')}`;
            }
        }
        await this.nativeQuery(sql,params)
    }
    async destroyOne(tablename: string, where: any, options: any): Promise<any> {
        let sql = `DELETE FROM \`${tablename.toLowerCase()}\``;
        let whereParam = [];
        let params = {}
        if(where){
            for(let k in where){
                let kname =  k.replace(/\%/g,'');
                if(where[k] === undefined){
                    continue
                }
                if(k.startsWith("%")){
                    whereParam.push(`\`${kname}\` LIKE :${kname}`);
                }else{
                    whereParam.push(`\`${kname}\`=:${kname}`);
                }
                params[kname] = where[k];
            }
            if(whereParam.length){
                sql += ` WHERE ${whereParam.join(' AND ')}`;
            }
        }
        sql += " LIMIT 1";
        await this.nativeQuery(sql,params)
    }
    nativeQuery(query: string,params?:any): Promise<any> {
        return new Promise((d, r) => {
            this.conn.getConnection(function (err, conn) {
                if(err){
                    r({ query, err })
                }
                conn.config.queryFormat = function (query, values) {
                    if (!values) return query;
                    return query.replace(/\:(\w+)/g, function (txt, key) {
                        if (values.hasOwnProperty(key)) {
                            return this.escape(values[key]);
                        }
                        return txt;
                    }.bind(this));
                };
                conn.query(query, params, function (error, results, fields) {
                    if (error) {
                        r({ query, error });
                        return;
                    }
                    d(results);
                    conn.release();
                    //console.log('The solution is: ', results[0].solution);
                });
            })
        })
    }

    createTable(name: string, fields: any): Promise<any> {
        return new Promise(async (d: Function) => {
            let drop = `DROP TABLE IF EXISTS \`${name.toLowerCase()}\``;
            try {
                await this.nativeQuery(drop);
            } catch (e) {
                console.error(e)
            }
            let uniqueGroup = {};
            let primaryKey = []
            let sql = `CREATE TABLE \`${name.toLowerCase()}\`(`;
            let flds = [];
            for (let name in fields) {
                const options = fields[name];
                
                let f = ` \`${name}\` `;
                if(options.customType){
                    f += options.customType;
                    flds.push(f);
                    continue;
                }
                switch (options.type) {
                    case 'Number':
                        f += ` INT(11) `;
                        break;
                    case 'MText':
                        f += ` TEXT `;
                        break;
                    case 'Date':
                        f += ` TIMESTAMP `;
                        break;
                    case 'String':
                        f += ` VARCHAR(255) `;
                        break;
                    case 'Boolean':
                        f += ` BOOLEAN `;
                        break;
                    case 'MJSON':
                        f += ` TEXT `;
                        break;
                    default:
                        console.warn(`type [${options.type}] not reconize`);
                        break;
                }
                if (options.primary) {
                    primaryKey.push(` \`${name}\` `);
                    //f += " PRIMARY KEY ";
                }
                if (options.autoIncrement) {
                    f += " auto_increment ";
                }
                if (options.unique) {
                    f += " unique ";
                }
                if(options.uniqueGroup){
                    if(!uniqueGroup[options.uniqueGroup]){
                        uniqueGroup[options.uniqueGroup] = [];
                    }
                    uniqueGroup[options.uniqueGroup].push(` \`${name}\` `);
                }
                flds.push(f);
            };
            if(Object.keys(uniqueGroup).length){
                Object.keys(uniqueGroup).forEach(k=>{
                    flds.push(`UNIQUE KEY \`${k}\` (${uniqueGroup[k].join(",")})`)
                })
            }
            
            if(primaryKey.length){
                flds.push(`PRIMARY KEY (${primaryKey.join(",")})`);
            }

            sql += `${flds.join(',\n')} )`;

            try {
                await this.nativeQuery(sql);
            } catch (e) {
                console.error(e)
            }
            d();
        })
    }

}