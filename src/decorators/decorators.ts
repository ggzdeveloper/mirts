import 'reflect-metadata';
import { FieldOptions } from '../interface/fieldOptions';
import { RouteOptions } from '../interface/routeOptions';

export function field(options?:FieldOptions) {
    return (target: any, propertyName: string)=>{
        let _val = target[propertyName];

        var t = Reflect.getMetadata("design:type", target, propertyName);
        
        let type = t.name;

        const getter = () => {
            return _val;
        };

        const setter = newVal => {
            _val = newVal;
        };
        Object.defineProperty(target, propertyName, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });

        
        if (!target.__fields__) {
            target.__fields__ = {};
        }
        
        target.__fields__[propertyName] = {
            type,
            ...options
        };
        appendFieldMetadata({[propertyName]:{type,...options}},target);
    }
}

export function entity(){
    return function <T extends {new(...args:any[]):{}}>(constructor:T) {
        //constructor.__proto__.__entity=true;
        let fields = getFieldsMetadata(new constructor({},{isNewRecord:false}));
        //console.log(constructor.name);
        //constructor.__proto__[constructor.name] = fields;
        //constructor.__proto__.fields = fields;
        //console.log(constructor.name,fields);
        //return (new constructor()).constructor
        return class extends constructor {
            private __entity:boolean = true;
            private __fields = fields;
        }
    }
}

export function Route(options:RouteOptions){
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        appendMethodMetadata({[propertyKey]:{...options}},target);
        /*
        let _val = target[propertyName];

        var t = Reflect.getMetadata("design:type", target, propertyName);
        
        //console.log(`${propertyName} type: ${t.name}`);
        let type = t.name;
        
        if (!target.__fields__) {
            target.__fields__ = {};
        }
        
        target.__fields__[propertyName] = {
            type,
            ...options
        };
        /*
        Reflect.defineMetadata("model:fields",target.__fields__,target);*/
        //appendFieldMetadata({[propertyName]:{type,...options}},target);
    }
}

export function Controller(opt:{path:string,permissions?:any,guards?:any}){

    return function <T extends {new(...args:any[]):{}}>(constructor:T) {
        //constructor.__proto__.__controller=true;
        let fields = getMethodMetadata(new constructor());
        return class extends constructor {
            private __controller:boolean = true;
            private __path:string = opt.path;
            private __MethodPath = fields;
            private __permissions = opt.permissions || ['NONE'];
            private __guards = opt.guards || [];

            __render(req,res){
                res.send(`<h1>${constructor.name}</h1>`);
            }
        }
    }
}


export function getFieldsMetadata(target:any):any{
    return Reflect.getMetadata("model:fields", target);
}

export function getMethodMetadata(target:any):any{
    return Reflect.getMetadata("model:Method", target);
}

function appendMethodMetadata(value:any,target:any):void{
    let name = "model:Method";
    let data = Reflect.getMetadata(name, target);
    if(!data){
        data = {};
    }
    //target.constructor.prototype.Method = {...data,...value};
    Reflect.defineMetadata(name,{...data,...value},target);
}

function appendFieldMetadata(value:any,target:any):void{
    let name = "model:fields";
    let data = Reflect.getMetadata(name, target);
    if(!data){
        data = {};
    }
    target.constructor.prototype.fields = {...data,...value};
    Reflect.defineMetadata(name,{...data,...value},target);
}