import * as fs from "fs";
import * as path from "path";
import { Model } from '../models/Model';
import * as util from 'util';
import 'reflect-metadata';
import { getFieldsMetadata } from "../decorators/decorators";
import { Migrate } from "./exportModels";

export function getAllModels(dir): Array<Model> {
    let files = fs.readdirSync(path.join(dir));
    let models = [];
    files.forEach(file => {
        if (fs.lstatSync(path.join(dir, file)).isFile() && (path.join(dir, file).endsWith(".js") || path.join(dir, file).endsWith(".ts"))) {
            if (!path.parse(file).name.endsWith(".d")) {
                let model = require(path.join(dir, file));
                let dbModel = model.default;
                if (dbModel) {
                    let instance = new dbModel({},{isNewRecord:false});
                    if (instance.__entity && instance instanceof Model) {
                        //delete 
                        models.push({
                            name: path.parse(file).name,
                            model: dbModel,
                            fields: getFieldsMetadata(instance)
                        })
                    } else {
                        console.warn(`error in model ${path.parse(file).name}, add @entity to class and extends of Model`);
                    }
                } else {
                    console.warn(`error in model ${path.parse(file).name} use "export default" `);
                }
            }
        }
    });
    return models;
}
