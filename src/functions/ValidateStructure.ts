import * as fs from 'fs';
import * as path from 'path';

const structure: any = {

    models: {},
    plugins: {},
    controllers: {},
    configs:{},
    middleware:{},
}
export function validStructure(dir, struct?): boolean {
    if (!struct) {
        struct = structure;
    }
    for (let k in struct) {
        if (fs.existsSync(path.resolve(dir, k))) {
            if (Object.values(struct[k]).length) {
                return validStructure(path.resolve(dir, k), struct[k]);
            }
        } else {
            //console.warn(`folder ${k} is no exists path: ${path.resolve(dir, k)}`);
        }
    }
    return true;
}
