import * as util from 'util';

export async function Migrate(models, driver, type) {
    
    for(let k in models){
        let m = models[k];
        switch (type) {
            case 'alter':
                await createMigration(driver, m);
                break;
            case 'drop':
                await createTable(driver, m)
                break;
            case 'safe':

                break;
        }
    }
}

async function createMigration(driver,model) {
	let flds = model.model._getFieldCreate();
	await model.model.init(flds,{ 
		sequelize:model.model.datastorage, modelName: model.name.toLowerCase(),
		// I don't want createdAt
		createdAt: false,
		// I want updatedAt to actually be called updateTimestamp
		updatedAt: false
	})
    let record
    try{
		record = await model.model.findAll({});
    }catch(e){
		console.error(e);
    }
    
    await createTable(driver, model);
    
    if(record && Array.isArray(record)){
        for (let index = 0; index < record.length; index++) {
            const element = record[index].dataValues;
            model.model.create(element);
        }
    }
}

async function createTable(driver, model) {
	await model.model.drop();
	await model.model.sync()
}