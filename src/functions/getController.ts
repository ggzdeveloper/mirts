import * as fs from "fs";
import * as path from "path";
import 'reflect-metadata';

export function getAllController(dir): Array<any> {
    let files = fs.readdirSync(path.join(dir));
    let models = [];
    files.forEach(file => {
        if (fs.lstatSync(path.join(dir, file)).isFile() && (path.join(dir, file).endsWith(".js") || path.join(dir, file).endsWith(".ts"))) {
            if(!path.parse(file).name.endsWith(".d")){
                let model = require(path.join(dir, file));
                let dbModel = model.default;
                if(dbModel){
                    let instance = new dbModel();
                    if(instance.__controller){
                        //delete 
                        models.push({
                            name:path.parse(file).name,
                            instance,
                        })
                    }else{
                        console.warn(`error in model ${path.parse(file).name}, add @Controller to class`);
                    }
                }else{
                    console.warn(`error in model ${path.parse(file).name} use "export default" `);
                }
            }
            
        }
    });
    return models;
}