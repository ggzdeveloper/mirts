import * as fs from "fs";
import * as path from "path";

export function getAllMiddlewares(dir): Array<any> {
    let files = fs.readdirSync(path.join(dir));
    let middlewares = [];
    files.forEach(file => {
        if (fs.lstatSync(path.join(dir, file)).isFile() && (path.join(dir, file).endsWith(".js") || path.join(dir, file).endsWith(".ts"))) {
            if(!path.parse(file).name.endsWith(".d")){
				let model = require(path.join(dir, file));
				let dbModel = model.default;
				if (dbModel) {
					middlewares.push(dbModel);
				} else {
					console.warn(`error in middleware ${path.basename(file, ".js")} use "export default" `);
				}
            }
            
        }
    });
    return middlewares;
}