import { field, entity, Controller, Route } from "./decorators/decorators";
import { Model } from './models/Model';
import { MText, MJSON } from './models/Types';
import { MysqlStorage } from "./datastorage/MysqlStorage";
import * as fs from 'fs';
import * as path from 'path';
import { getAllModels } from "./functions/ValidateModels";
import { Migrate } from "./functions/exportModels";
import HttpServer from "./plugins/httpServer";
import { getAllController } from "./functions/getController";
import { Sequelize } from 'sequelize';
import { getAllMiddlewares } from "./functions/getMiddlewares";
declare var global: any;



export default class tsnodejs {
    private dirWorking: string = "";
    public static Connections: any = {};
    public static readonly configs: any = {};
    async init() {
        tsnodejs.addConection('mysql', MysqlStorage);
        this.dirWorking =  process.cwd();
        await this.processDirWorking();
        await this.getConfigurations();
        await this.initDataStorage();
        await this.initHttpServer();
    }

    async processDirWorking(){
        let dirConfig = path.resolve(this.dirWorking,"tsnodejs.json");
        if(fs.existsSync(dirConfig)){
            let fconfig:any = fs.readFileSync(dirConfig,{encoding:'utf8'});
            fconfig = JSON.parse(fconfig);
            if(fconfig.dirWorking){
                this.dirWorking = path.resolve(this.dirWorking,fconfig.dirWorking);
            }
        }
    }

    async getConfigurations() {
        let dir = this.dirWorking + "/configs/"
        let files = fs.readdirSync(path.join(dir));
        let configs = {};
        files.forEach(file => {
            if (fs.lstatSync(path.join(dir, file)).isFile() && (path.join(dir, file).endsWith(".js") || path.join(dir, file).endsWith(".ts"))) {
                if(!path.parse(file).name.endsWith(".d")){
                    let configFiles = require(path.join(dir, file));
                    let conf = configFiles.default;
                    let name = file.substring(0, file.length - 3);
                    configs[name] = conf;
                }
            }
        });
        Object.assign(tsnodejs.configs, configs);
    }

    async initHttpServer(){
        let http = new HttpServer(tsnodejs.configs.http);
        await http.init();
		let middlewares = getAllMiddlewares(this.dirWorking + "/middlewares/");
		middlewares.forEach(e=>{
			http.addMiddeleware(e);
		})
        let controllers = getAllController(this.dirWorking + "/controllers/");
        controllers.forEach(e=>{
            /* http.addRoute('get',`/${e.instance.__path}`,e.instance.__render); */
            for(let k in e.instance.__MethodPath){
                let __permissions = JSON.parse(JSON.stringify(e.instance.__permissions));
                let conf = e.instance.__MethodPath[k];
                
                if(conf.permissions){
                    conf.permissions.forEach(e=>{
                        __permissions.push(e);
                    })
                }
                http.addRoute(conf.method,`/${e.instance.__path}/${conf.path}`,e.instance[k],__permissions);
            }
        })
    }

    async initDataStorage() {
        let dir = this.dirWorking + "/models/"
        let datastorageConfig = tsnodejs.configs.datastorage;
        if (datastorageConfig && datastorageConfig.driver && datastorageConfig.driver != "none") {
            let models = getAllModels(dir);
            let driverName = datastorageConfig.driver;

            if (!tsnodejs.Connections[driverName]) {
                console.error("Driver not found, datastorage disabled");
            } else {
				let con = tsnodejs.Connections[driverName];
                let driver = new Sequelize({
					dialect:'mysql',
					host:datastorageConfig.host,
					username:datastorageConfig.user,
					password:datastorageConfig.password,
					database:datastorageConfig.database,
					port:datastorageConfig.port,
					logging:datastorageConfig.logging || false,
					pool: datastorageConfig.pool || {
						max: 10,
						min: 0,
						acquire: 30000,
						idle: 10000
					}
				})
                Model.datastorage = driver;
                try {
                    await Migrate(models, driver, datastorageConfig.migrate);
                } catch (ex) {
                    console.error(ex);
                    throw ex;
                }
            }
        }
    }



    public static async addConection(driver, datastorage) {
        this.Connections[driver] = datastorage;
    }
}

export { field, Model, MText, MJSON, entity, Controller, Route }