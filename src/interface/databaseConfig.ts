export interface DatabaseConfig{
    connectionLimit: number, //important
    host: string,
    user: string,
    password: string,
    database: string,
    port?: number
}