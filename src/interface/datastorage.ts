export interface datastorage{
    connect(options:any):Promise<any>
    find(tablename:string, where: any,options:any):Promise<Array<any>>
    findOne(tablename:string, where: any,options:any):Promise<any>
    create(tablename:string, options: any):Promise<any>
    update(tablename:string, where: any,options:any):Promise<any>
    destroy(tablename:string, where: any,options:any):Promise<any>
    destroyOne(tablename:string, where: any,options:any):Promise<any>
    nativeQuery(query:string,params?:any):Promise<any>
    createTable(name:string,fieds:any):Promise<any>
    count(tablename:string):Promise<number>
}