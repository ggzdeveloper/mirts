export interface FieldOptions {
    primary?:boolean,
    autoIncrement?:boolean,
    unique?:boolean,
    uniqueGroup?:string,
    model?:string,
    customType?:string,
}