export interface HttpConfig{
    port:number,
    middleware?:Array<Function>,
    middlewareOrder?:Array<string>,
    extendConfig?:Function,
    log?:Enumerator<["INFO","LOG","WARNING","ERROR"]>
}