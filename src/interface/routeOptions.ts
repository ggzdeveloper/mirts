export interface RouteOptions {
    path:string,
    method:string,
    permissions?:Array<String>,
    guards?:Array<Function>
}