import { Sequelize, Model as sqlModel, DataTypes } from 'sequelize';

export class Model extends sqlModel{
    static datastorage:Sequelize
    static __proto__: any;
    static __fields: any;
    static __entity: any;

	public static _getFieldCreate(){
		let instance:any = new this({},{isNewRecord:false});
        let fields = instance.__fields;
		let flds = {

		}
		for (let name in fields) {
			const options = fields[name];
			flds[name] = {

			}
			
			if(options.customType){
				flds[name].type = options.customType;
				continue;
			}
			switch (options.type) {
				case 'Number':
					flds[name].type = DataTypes.INTEGER;
					break;
				case 'MText':
					flds[name].type = DataTypes.TEXT({length:'long'});
					break;
				case 'Date':
					flds[name].type = DataTypes.DATE;
					break;
				case 'String':
					flds[name].type = DataTypes.STRING;
					break;
				case 'Boolean':
					flds[name].type = DataTypes.BOOLEAN;
					break;
				case 'MJSON':
					flds[name].type = DataTypes.TEXT({length:'long'});
					break;
				default:
					console.warn(`type [${options.type}] not reconize`);
					continue;
					break;
			}
			if (options.primary) {
				flds[name].primaryKey = true;
				//f += " PRIMARY KEY ";
			}
			if (options.autoIncrement) {
				flds[name].autoIncrement = true;
			}
			if (options.unique) {
				flds[name].unique = true;
			}
			if(options.uniqueGroup){
				flds[name].unique =  options.uniqueGroup;
			}
		};
		return flds
	}
}