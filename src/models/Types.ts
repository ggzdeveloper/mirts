/**
 * Column definition Text
 */
export class MText{
    static set(value){
        return value;
    }
    static get(value){
        return value;
    }
}

/**
 * Column definition JSON
 */
export class MJSON{
    static set(value){
        return JSON.stringify(value || "null");
    }
    static get(value){
        return JSON.parse(value);
    }
}
