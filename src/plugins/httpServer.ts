import * as  express from 'express';


export default class HttpServer {
    private config: any = {};
    private routes: any = {};
    private app: any;

    constructor(config) {
        this.config = config;
        this.app = express();
		if(typeof config.getApp == "function"){
			config.getApp(this.app);
		}
    }

    init() {
        return new Promise((d:Function, r) => {
            let port = this.config.port;
            let server = this.app.listen(port, function () {
                console.log(`Server listing by port ${port}!`);
                d();
            });
            if (this.config.middleware) {
                this.config.middleware.forEach(md => {
                    this.app.use(md);
                });
            }
            if (typeof this.config.extendConfig == "function") {
                this.config.extendConfig(this.app, server)
            }
            this.app.use(function (req, res, next) {
                /* console.log(`[${req.method}] ${new Date()} ${req.originalUrl}`); */
                if(!req.permissions){
                    req.permissions=['NONE'];
                }else{
                    if(req.permissions.indexOf("NONE")==-1){
                        req.permissions = ["NONE", ...req.permissions];
                    }
                }
                res.jsonOk = function (resp) {
                    res.json({
                        success: true,
                        code: 200,
                        message: '',
                        response: resp
                    })
                }
                res.jsonError = function (message, code) {
                    res.status(code).json({
                        success: false,
                        code: code,
                        message: message,
                        response: null
                    })
                }
                next()
            })
            this.app.use(function (err, req, res, next) {
                console.error(err.stack);
                res.status(500).send('Something broke!');
            });
        })
    }

    addMiddeleware(middleware) {
        this.app.use(middleware)
    }
    addRoute(method, path, fn, permissions = ['NONE']) {
        /* console.info(path); */
        switch (method) {
            case "GET":
            case "get":
                this.app.get(path, function (req, res) {
                    let noPermissions = false;
                    permissions.forEach(e => {
                        if (req.permissions.indexOf(e) == -1) {
                            noPermissions = true;
                        }
                    })
                    if (noPermissions) {
                        return res.jsonError("E_NOT_PERMISSION", 403)
                    }
                    fn(req, res);
                });
                break;
            case "POST":
            case "post":
                this.app.post(path, function (req, res) {
                    let noPermissions = false;
                    permissions.forEach(e => {
                        if (req.permissions.indexOf(e) == -1) {
                            noPermissions = true;
                        }
                    })
                    if (noPermissions) {
                        return res.jsonError("E_NOT_PERMISSION", 403)
                    }
                    fn(req, res);
                });
                break;
            case "PUT":
            case "put":
                this.app.put(path, function (req, res) {
                    let noPermissions = false;
                    permissions.forEach(e => {
                        if (req.permissions.indexOf(e) == -1) {
                            noPermissions = true;
                        }
                    })


                    if (noPermissions) {
                        return res.jsonError("E_NOT_PERMISSION", 403)
                    }
                    fn(req, res);
                });
                break;
            case "DELETE":
            case "delete":
                this.app.delete(path, function (req, res) {
                    let noPermissions = false;
                    permissions.forEach(e => {
                        if (req.permissions.indexOf(e) == -1) {
                            noPermissions = true;
                        }
                    })


                    if (noPermissions) {
                        return res.jsonError("E_NOT_PERMISSION", 403)
                    }
                    fn(req, res);
                });
                break;
        }
    }
}
